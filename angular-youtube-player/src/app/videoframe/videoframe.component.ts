import { Component, OnInit, Input} from '@angular/core';
import { Video } from '../video';

import {DomSanitizer, SafeUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-videoframe',
  templateUrl: './videoframe.component.html',
  styleUrls: ['./videoframe.component.css']
})
export class VideoframeComponent implements OnInit {
  welcome="Welcome on Youtube Player";
  demo="Type the URL of the embedded Youtube video you want to watch.";
  video: Video = {
     url: 'https://www.youtube.com/embed/dsUXAEzaC3Q'
   };

  constructor(
    public sanitizer : DomSanitizer
  ) {
  }

  ngOnInit() {
  }

  getSanitizedUrl(){
    if(this.video.url.search("https://www.youtube.com/")!=-1){
        this.video.url=this.video.url.replace("watch?v=", "embed/");
    }
    else{
      this.video.url="https://www.youtube.com/embed/"+this.video.url;
    }
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.video.url);
  }

}
