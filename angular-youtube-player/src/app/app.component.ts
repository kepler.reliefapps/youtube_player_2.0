import { Component,AfterViewInit, ViewChild } from '@angular/core';
import { Video } from './video';
import { VideoService } from './video.service';

@Component({
  selector: 'app-root',
  templateUrl: `./app.component.html`,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  videos: Video[];
  bookmarkslist: Video[];
  nbrBm: number;
  @ViewChild('bar') bar;
  @ViewChild('history') history;
  @ViewChild('frame') frame;
  @ViewChild('bookmarks') bookmarks;
  ngOnInit(){
    this.getVideos();
    this.getBookmarks();
    this.nbrBm=this.bookmarkslist.length;

  }

constructor(private videoService: VideoService) { }

getVideos(): void {
  this.videoService.getVideos().subscribe(videos => this.videos = videos);
}

getBookmarks(): void {
  this.videoService.getBookmarks().subscribe(bookmarkslist => this.bookmarkslist = bookmarkslist);
}

  addBm(url: string): void {
    this.videoService.addBookmark({ url } as Video);
    this.getBookmarks();
    this.nbrBm=this.bookmarkslist.length;
  }

handleh(url: string){
  this.frame.video.url=url;
}

  handlesb(url:string) {
    this.videoService.addVideo({ url } as Video);
    this.videos.push({ url } as Video);
    this.frame.video.url=url;
  }

  handlebm(url: string){
    this.frame.video.url=url;
  }
}
