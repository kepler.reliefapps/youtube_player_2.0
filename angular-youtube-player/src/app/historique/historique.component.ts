import { Component, OnInit, Input,Output,EventEmitter} from '@angular/core';
import { Video } from '../video';

@Component({
  selector: 'app-historique',
  templateUrl: './historique.component.html',
  styleUrls: ['./historique.component.css']
})
export class HistoriqueComponent implements OnInit {
@Output() histo: EventEmitter<string>=new EventEmitter();
@Input() videos: Video[];
  history="History";

onSelect(video: string){
  this.histo.emit(video);
}
constructor() { }


ngOnInit() {

}

}
