import { Component, OnInit,Output,Input, EventEmitter } from '@angular/core';
import { Video } from '../video';
import { VideoService } from '../video.service';
@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {


  @Output() bmarks: EventEmitter<string>=new EventEmitter();
  @Output() display: EventEmitter<Video[]>=new EventEmitter();
  @Output() add: EventEmitter<string>=new EventEmitter();
  @Input() bookmarkslist: Video[];
  @Input() toAdd: Video;
  @Input() nbrBm: number;


addBookmarkbutton="Add a bookmark";
displayBookmarksbutton="Display bookmarks";
visible: boolean =true;


  constructor(private videoService: VideoService) { }

  ngOnInit() {

  }

  onSelect(video: string){
    this.bmarks.emit(video);
  }

  displayFct(){
    this.visible = !this.visible;
  }

  addFct(){
    this.add.emit(this.toAdd.url);
  }
}
