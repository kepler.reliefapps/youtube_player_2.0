import { Video } from './video';

export const VIDEOS: Video[] =
 [
  {url: 'https://www.youtube.com/embed/jUe8uoKdHao'},
  {url: 'https://www.youtube.com/embed/KpsUsP2lIzU'},
  {url: 'https://www.youtube.com/embed/EeGyydeT_8U'},
  {url: 'https://www.youtube.com/embed/GulJXSGJp7s'}
];
