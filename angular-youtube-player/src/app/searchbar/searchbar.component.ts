import { Component, OnInit, Output,EventEmitter} from '@angular/core';
import { Video } from '../video';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {

@Output() search: EventEmitter<string>=new EventEmitter();

title="Youtube Player";

  constructor() { }

  ngOnInit() {
  }

  submitURL(url:string){
    this.search.emit(url);
  }

}
