import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appSubmitUrl]'
})
export class SubmitUrlDirective {

  constructor(el: ElementRef) {
      el.nativeElement.style.backgroundColor = 'yellow'
   }

}
