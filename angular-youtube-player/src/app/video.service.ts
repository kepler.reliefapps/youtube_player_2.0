import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Video} from './video';
//import { VIDEOS} from './mock-videos';

@Injectable()
export class VideoService {

  constructor() { }

  getVideos(): Observable<Video[]> {
    if (typeof(Storage) !== "undefined") {
      //localStorage.removeItem("history");
      //localStorage.history=JSON.stringify(VIDEOS);

    var videos=JSON.parse(localStorage.getItem("history"));
} else {
    // Sorry! No Web Storage support..
}
    return of(videos);
  }

addVideo(video: Video):Observable<Video[]>{
  if (typeof(Storage) !== "undefined") {
  var videos=JSON.parse(localStorage.getItem("history"));
  videos.push(video);
  localStorage.history=JSON.stringify(videos);
} else {
  // Sorry! No Web Storage support..
}
return of(videos);
}

getBookmarks(): Observable<Video[]> {
  if (typeof(Storage) !== "undefined") {
    //localStorage.removeItem("bookmarks");
    //localStorage.bookmarks=JSON.stringify(VIDEOS);
  var videos=JSON.parse(localStorage.getItem("bookmarks"));
} else {
  // Sorry! No Web Storage support..
}
  return of(videos);
}


addBookmark(video: Video):Observable<Video[]>{
  if (typeof(Storage) !== "undefined") {
  var videos=JSON.parse(localStorage.getItem("bookmarks"));
  videos.push(video);
  localStorage.bookmarks=JSON.stringify(videos);
} else {
  // Sorry! No Web Storage support..
}
return of(videos);
}
}
