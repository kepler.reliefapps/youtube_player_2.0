import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { HistoriqueComponent } from './historique/historique.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { VideoframeComponent } from './videoframe/videoframe.component';
import { VideoService } from './video.service';
import { SubmitUrlDirective } from './submit-url.directive';

@NgModule({
  declarations: [
    AppComponent,
    SearchbarComponent,
    HistoriqueComponent,
    BookmarksComponent,
    VideoframeComponent,
    SubmitUrlDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [VideoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
